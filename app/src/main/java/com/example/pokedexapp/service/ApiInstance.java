package com.example.pokedexapp.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiInstance {

    private static Retrofit retroInstance=null;
    private static String baseURL="https://pokeapi.co/api/v2/";

    public static IPokeApi getService(){

        if(retroInstance==null){
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();
            retroInstance=new Retrofit.Builder().baseUrl(baseURL).addConverterFactory(GsonConverterFactory.create(gson)).build();
        }
        return retroInstance.create(IPokeApi.class);

    }



    public static String getBaseURL() {
        return baseURL;
    }
}
