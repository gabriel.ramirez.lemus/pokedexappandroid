package com.example.pokedexapp.service;

import com.example.pokedexapp.model.Pokemon;
import com.example.pokedexapp.model.Result;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface IPokeApi {

    @GET("pokemon/")
    Call<Result> getPokemons(@Query(value = "offset",encoded = true) int offset,@Query(value = "limit",encoded = true) int limit);

    @GET("pokemon/{id_pokemon}")
    Call<Pokemon> getPokemonDetails(@Path("id_pokemon") String id);



}
