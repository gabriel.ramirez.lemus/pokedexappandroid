package com.example.pokedexapp.model.pokemondetails;

import com.example.pokedexapp.model.Pokemon;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ThirdGen {
    @SerializedName("emerald")
    @Expose
    private EmeraldImage emeraldImage;


    public EmeraldImage getEmeraldImage() {
        return emeraldImage;
    }
}
