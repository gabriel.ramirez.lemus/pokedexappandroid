package com.example.pokedexapp.model.pokemondetails;

import com.example.pokedexapp.model.Pokemon;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Sprite {
    @SerializedName("versions")
    @Expose
    private Version version;

    public Version getVersion() {
        return version;
    }
}
