package com.example.pokedexapp.model.pokemondetails;

import com.example.pokedexapp.model.Pokemon;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Version {
    @SerializedName("generation-iii")
    @Expose
    private ThirdGen thirdGen;

    public ThirdGen getThirdGen() {
        return thirdGen;
    }
}
