package com.example.pokedexapp.model;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.example.pokedexapp.service.ApiInstance;
import com.example.pokedexapp.service.IPokeApi;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PokemonRepository {

    private List<Pokemon> pokemonList=new ArrayList<>();

    private MutableLiveData<List<Pokemon>> mutableLiveData=new MutableLiveData<>();

    private int offset=0;
    private int limit=20;



    public MutableLiveData<List<Pokemon>>getMutableLiveData(){
        loadPokemonData();
        return mutableLiveData;
    }

    public void updatePokemonList(){
        offset=offset+limit;
        loadPokemonData();
        // se llama metodo que hace la consulta a API Pokemon

    }

    public void loadPokemonData(){
        IPokeApi iPokeApi= ApiInstance.getService();
        Call<Result> resultCall=iPokeApi.getPokemons(offset,limit);

        resultCall.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                Result result=response.body();

                if(response!=null && result.getResults()!=null){

                    List<ResultItem> pokemons=result.getResults();
                    int i=0;
                    retrievePokemonDetail(iPokeApi,pokemons,i);
                }
            }
            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                Log.i("errorsito",t.getLocalizedMessage());
            }
        });



    }

    public void retrievePokemonDetail(IPokeApi iPokeApi,List<ResultItem> resultItems,int i){

        String baseUrl=ApiInstance.getBaseURL();
        String idPokemonUrl=resultItems.get(i).getUrl().replace(baseUrl+"pokemon/","");
        Call<Pokemon> pokemonCall=iPokeApi.getPokemonDetails(idPokemonUrl);

        pokemonCall.enqueue(new Callback<Pokemon>() {
            @Override
            public void onResponse(Call<Pokemon> call, Response<Pokemon> response) {
                Pokemon pokemonDetail=response.body();

                if(response!=null && pokemonDetail!=null){


                    pokemonList.add(pokemonDetail);
                    if(i==limit-1){
                        //offset+=limit;
                        //pokemonList.addAll(newPokemonList);

                        mutableLiveData.setValue(pokemonList);
                    }else{

                        retrievePokemonDetail(iPokeApi,resultItems,i+1);
                    }

                }
            }

            @Override
            public void onFailure(Call<Pokemon> call, Throwable t) {
                Log.i("errorsito2",t.getLocalizedMessage());
            }
        });
    }

}
