package com.example.pokedexapp.model;

import android.widget.ImageView;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.example.pokedexapp.BR;
import com.example.pokedexapp.model.pokemondetails.Sprite;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Pokemon extends BaseObservable {

    @Bindable
    @SerializedName("id")
    @Expose
    private String pokemonID;


    @Bindable
    @SerializedName("name")
    @Expose
    private String name;

    @Bindable
    @SerializedName("sprites")
    @Expose
    private Sprite sprite;

    public String getPokemonID() {
        return pokemonID;
    }

    public void setPokemonID(String pokemonID) {
        this.pokemonID = pokemonID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Sprite getSprite() {
        return sprite;
    }

    public void setSprite(Sprite sprite) {
        this.sprite = sprite;
    }

    @BindingAdapter({"imgPokemon"})
    public static void cargaImgPokemon(ImageView img , String url){
        Glide.with(img.getContext())
                .load(url)
                .into(img);
   }


}
