package com.example.pokedexapp.model.pokemondetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EmeraldImage {
    @SerializedName("front_default")
    @Expose
    private String frontDefault;
    @SerializedName("front_shiny")
    @Expose
    private String frontShiny;

    public String getFrontDefault() {
        return frontDefault;
    }

    public String getFrontShiny() {
        return frontShiny;
    }
}
