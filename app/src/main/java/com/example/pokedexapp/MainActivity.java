package com.example.pokedexapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.pokedexapp.adapters.RecyclerPokedexAdapter;
import com.example.pokedexapp.databinding.ActivityMainBinding;
import com.example.pokedexapp.model.Pokemon;
import com.example.pokedexapp.model.PokemonRepository;
import com.example.pokedexapp.model.Result;
import com.example.pokedexapp.model.ResultItem;
import com.example.pokedexapp.service.ApiInstance;
import com.example.pokedexapp.service.IPokeApi;
import com.example.pokedexapp.viewmodel.PokemonViewModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private boolean firstUpdate=true;

    private RecyclerPokedexAdapter adapter;
    private ActivityMainBinding activityMainBinding;



    private PokemonViewModel pokemonViewModel;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityMainBinding= ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(activityMainBinding.getRoot());

        adapter=new RecyclerPokedexAdapter(this);
        activityMainBinding.recyclerPokemons.setLayoutManager(new LinearLayoutManager(this));
        activityMainBinding.setMyAdapter(adapter);

        pokemonViewModel= new ViewModelProvider(this).get(PokemonViewModel.class);
        pokemonViewModel.getPokemons().observe(this, new Observer<List<Pokemon>>() {
            @Override
            public void onChanged(List<Pokemon> pokemons) {


                if(firstUpdate){
                    mostrarLoader();
                    llenarRecycler(pokemons);
                    ocultarLoader();
                    firstUpdate=false;
                }else{
                    ocultarLoader();
                    llenarRecycler(pokemons);
                }

            }
        });



        activityMainBinding.recyclerPokemons.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager layoutManager = (LinearLayoutManager) activityMainBinding.recyclerPokemons.getLayoutManager();
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if(activityMainBinding.progressCircular.getVisibility()==View.GONE && (visibleItemCount+firstVisibleItemPosition)>=totalItemCount && firstVisibleItemPosition>=0 ){
                    mostrarLoader();
                    pokemonViewModel.updatePokeList();


                }
            }
        });




    }

    public void llenarRecycler(List<Pokemon> pokemons){

        adapter.setPokeList(pokemons);
        //Log.i("nombre_del_pokemon",pokemonList.get(0).getName());

    }

    public void mostrarLoader(){activityMainBinding.progressCircular.setVisibility(View.VISIBLE);}
    public void ocultarLoader(){activityMainBinding.progressCircular.setVisibility(View.GONE);}







}