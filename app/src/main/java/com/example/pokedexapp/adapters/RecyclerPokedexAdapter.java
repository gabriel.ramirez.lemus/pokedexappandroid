package com.example.pokedexapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pokedexapp.BR;
import com.example.pokedexapp.R;
import com.example.pokedexapp.databinding.PokemonItemBinding;
import com.example.pokedexapp.model.Pokemon;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class RecyclerPokedexAdapter extends RecyclerView.Adapter<RecyclerPokedexAdapter.ViewHolder> {

    private List<Pokemon> pokeList=new ArrayList<>();
    private Context context;

    public RecyclerPokedexAdapter( Context context) {

        this.context = context;
    }

    public void setPokeList(List<Pokemon> pokemons){
        this.pokeList=pokemons;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        PokemonItemBinding item = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.pokemon_item,parent,false);

        return new ViewHolder(item);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Pokemon pokemon=pokeList.get(position);
        holder.bind(pokemon);


    }

    @Override
    public int getItemCount() {
        return pokeList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        private PokemonItemBinding pokemonItemBinding;


        public ViewHolder(@NonNull PokemonItemBinding pokemonItemBinding) {
            super(pokemonItemBinding.getRoot());
            this.pokemonItemBinding=pokemonItemBinding;
        }

        public void bind(Object obj){
            pokemonItemBinding.setVariable(BR.pokemon,obj);
            pokemonItemBinding.executePendingBindings();
        }
    }
}
