package com.example.pokedexapp.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.pokedexapp.model.Pokemon;
import com.example.pokedexapp.model.PokemonRepository;

import java.util.List;

public class PokemonViewModel extends ViewModel {

    private PokemonRepository pokemonRepository;

    public PokemonViewModel() {
        this.pokemonRepository = new PokemonRepository();
    }

    public LiveData<List<Pokemon>> getPokemons(){

        return pokemonRepository.getMutableLiveData();
    }

    public void updatePokeList(){
        pokemonRepository.updatePokemonList();
    }

}
